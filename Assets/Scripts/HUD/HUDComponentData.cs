﻿using System.Collections;
using UnityEngine;
using Unity.Entities;
using Unity.Collections;

[GenerateAuthoringComponent]
public struct HUDComponentData : IComponentData
{
    public Entity HUDEntity;
}