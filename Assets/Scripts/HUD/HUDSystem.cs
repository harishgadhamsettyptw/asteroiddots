﻿using System.Collections;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class HUDSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        base.OnCreate();

        RequireSingletonForUpdate<GameStateData>();
        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        GameStateData gameStateData = GetSingleton<GameStateData>();

        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        Entities.ForEach((Entity e, int entityInQueryIndex, ref HUDComponentData hUDComponentData) =>
        {
            if (gameStateData.GameState == GameStates.InGame)
                ecb.AddComponent<Disabled>(entityInQueryIndex, hUDComponentData.HUDEntity);
        }).ScheduleParallel();

        Entities.WithAll<HUDComponentData>().ForEach((Entity e, int entityInQueryIndex, ref Disabled hUDComponentData) =>
        {
            if (gameStateData.GameState == GameStates.Start)
                ecb.RemoveComponent<Disabled>(entityInQueryIndex, e);
        }).ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }
}