using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AudioUtil
{
    public static void PlaySound(AudioClipTypeEnum clipType, bool loop = false)
    {
        AudioManager.Instance.PlayAudio(clipType);
    }
}