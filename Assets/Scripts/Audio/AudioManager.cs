using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum AudioClipTypeEnum
{
    BG_MUSIC,
    PLAYER_MOVE,
    PLAYER_FIRE,
    PLAYER_DEAD,
    SHIELD_UP,
    SHIELD_DOWN,
    ASTEROID_BLAST,
    UFO_MOVE,
    UFO_FIRE,
    GAME_OVER
}

public class AudioManager : Singleton<AudioManager>
{
    [Serializable]
    public class AudioClipTypePair
    {
        public AudioClipTypeEnum ClipType;
        public AudioClip Clip;
    }

    [Serializable]
    public class AudioSourceTypePair
    {
        public AudioClipTypeEnum ClipType;
        public AudioSource Source;
    }

    [SerializeField] protected List<AudioClipTypePair> m_AudioClipTypePair;

    [SerializeField] protected List<AudioSourceTypePair> m_AudioSourceTypePair;

    public void PlayAudio(AudioClipTypeEnum clipType)
    {
        AudioSource audioSource = GetAudioSource(clipType);
        audioSource.enabled = false;
        audioSource.enabled = true;
    }

    public void PlayContinuousAudio(AudioClipTypeEnum clipType)
    {
        AudioSource audioSource = GetAudioSource(clipType);
        if (audioSource.isPlaying)
            return;
        audioSource.enabled = false;
        audioSource.enabled = true;
    }

    private AudioSource GetAudioSource(AudioClipTypeEnum clipType)
    {
        foreach(AudioSourceTypePair audioSourceTypePair in m_AudioSourceTypePair)
        {
            if (audioSourceTypePair.ClipType == clipType)
                return audioSourceTypePair.Source;
        }
        return null;
    }

    private AudioClip GetAudioClip(AudioClipTypeEnum clipType)
    {
        foreach (AudioClipTypePair pair in m_AudioClipTypePair)
        {
            if (pair.ClipType == clipType)
                return pair.Clip;
        }

        return null;
    }

    private AudioSource GetFreeAudioSource()
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        foreach (AudioSource audioSource in audioSources)
        {
            if (!audioSource.isPlaying)
                return audioSource;
        }

        return gameObject.AddComponent<AudioSource>();
    }
}