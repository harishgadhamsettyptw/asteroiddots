using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct AsteroidPrefabData : IComponentData
{
    public Entity AsteroidPrefab;
    public float SpawnDelay;
    public float Timer;
}