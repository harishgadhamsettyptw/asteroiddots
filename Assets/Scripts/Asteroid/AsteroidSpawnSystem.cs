using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;
using Random = Unity.Mathematics.Random;

[UpdateBefore(typeof(TransformSystemGroup))]
public class AsteroidSjpawnSystem : SystemBase
{
    private BeginSimulationEntityCommandBufferSystem beginSimulationEntityCommandBufferSystem;
    private Random random;

    protected override void OnCreate()
    {
        RequireSingletonForUpdate<GameStateData>();
        RequireSingletonForUpdate<ScreenBoundaryData>();
        random = new Random(314159);
        beginSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        GameStateData gameStateData = GetSingleton<GameStateData>();
        if (gameStateData.GameState != GameStates.InGame)
            return;

        ScreenBoundaryData screenBoundaryData = GetSingleton<ScreenBoundaryData>();
        float minX = screenBoundaryData.MinX;
        float minY = screenBoundaryData.MinY;
        float maxX = screenBoundaryData.MaxX;
        float maxY = screenBoundaryData.MaxY;
        float minRange = screenBoundaryData.MinRange;
        float maxRange = screenBoundaryData.MaxRange;

        var ecb = beginSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
        float deltaTime = Time.DeltaTime;
        float randomFloat = random.NextFloat(0f, 360f);
        float randomSpeed = random.NextFloat(3f, 5f);
        float3 randomFloat3 = float3.zero;
        int randomInt = random.NextInt(0, 1000);

        if (randomInt % 4 == 0)
        {
            // Left
            randomFloat3 = random.NextFloat3(new float3(minX, minY, 0f), new float3(minX, maxY, 0f));
        }
        else if (randomInt % 3 == 0)
        {
            // Right
            randomFloat3 = random.NextFloat3(new float3(maxX, minY, 0f), new float3(maxX, maxY, 0f));
        }
        else if (randomInt % 2 == 0)
        {
            // Up
            randomFloat3 = random.NextFloat3(new float3(minX, maxY, 0f), new float3(maxX, maxY, 0f));
        }
        else
        {
            // Down
            randomFloat3 = random.NextFloat3(new float3(minX, minY, 0f), new float3(maxX, minY, 0f));
        }

        Entities.WithAll<ShouldSpawnTag>().ForEach((Entity e, int entityInQueryIndex, ref AsteroidPrefabData asteroidPrefabData, in Translation translation) =>
        {
            asteroidPrefabData.Timer -= deltaTime;
            if (asteroidPrefabData.Timer <= 0)
            {
                asteroidPrefabData.Timer = asteroidPrefabData.SpawnDelay;
                var newEntity = ecb.Instantiate(entityInQueryIndex, asteroidPrefabData.AsteroidPrefab);
                ecb.SetComponent(entityInQueryIndex, newEntity, new Translation
                {
                    Value = randomFloat3
                });
                ecb.SetComponent(entityInQueryIndex, newEntity, new Rotation
                {
                    Value = quaternion.RotateZ(randomFloat)
                });
                ecb.SetComponent(entityInQueryIndex, newEntity, new AsteroidMovementData
                {
                    AsteroidSpeed = randomSpeed, ColliderRadius = 0.5f, KillScore = 100
                });
                ecb.AddComponent<ShouldSpawnTag>(entityInQueryIndex, newEntity);
            }
        }).ScheduleParallel();
        beginSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }
}