using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Transforms;
using Random = Unity.Mathematics.Random;

[UpdateInGroup(typeof(InitializationSystemGroup))]
public class AsteroidMovementSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;

        Entities.ForEach((ref AsteroidMovementData asteroidMovementData, ref Translation translation, ref LocalToWorld localToWorld) => {
            float3 direction = localToWorld.Up * deltaTime * asteroidMovementData.AsteroidSpeed;
            translation.Value += direction;
        }).ScheduleParallel();
    }
}