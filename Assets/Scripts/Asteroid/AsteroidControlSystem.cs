using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[UpdateInGroup(typeof(InitializationSystemGroup))]
public class AsteroidControlSystem : SystemBase
{
    private EndInitializationEntityCommandBufferSystem endInitializationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        endInitializationEntityCommandBufferSystem = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        EntityCommandBuffer entityCommandBuffer = endInitializationEntityCommandBufferSystem.CreateCommandBuffer();
        EntityQuery entityQuery = EntityManager.CreateEntityQuery(typeof(AsteroidPrefabData));

        entityCommandBuffer.AddComponent<ShouldSpawnTag>(entityQuery);
    }
}