using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Random = Unity.Mathematics.Random;

[GenerateAuthoringComponent]
public struct AsteroidMovementData : IComponentData
{
    public float AsteroidSpeed;
    public float ColliderRadius;

    public int KillScore;
}