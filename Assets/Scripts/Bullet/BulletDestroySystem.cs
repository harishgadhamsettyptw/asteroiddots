using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections;

public class BulletDestroySystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        RequireSingletonForUpdate<ScreenBoundaryData>();
    }

    protected override void OnUpdate()
    {
        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
        float deltaTime = Time.DeltaTime;
        ScreenBoundaryData screenBoundaryData = GetSingleton<ScreenBoundaryData>();
        float minX = screenBoundaryData.MinX, minY = screenBoundaryData.MinY, maxX = screenBoundaryData.MaxX, maxY = screenBoundaryData.MaxY, minRange = screenBoundaryData.MinRange, maxRange = screenBoundaryData.MaxRange;

        int length = 0;
        int counter = 0;
        Entity asteroidEntityToBeRemoved = default;
        Entity bulletEntityToBeRemoved = default;

        Entities.WithAll<BulletColliderData>().ForEach((Entity e) => { length++; }).WithoutBurst().Run();

        NativeArray<float> bulletColliderRadiusArray = new NativeArray<float>(length, Allocator.TempJob);
        NativeArray<float3> bulletColliderPositionArray = new NativeArray<float3>(length, Allocator.TempJob);
        NativeArray<Entity> bulletEntityArray = new NativeArray<Entity>(length, Allocator.TempJob);

        Entities.WithAll<BulletColliderData>().ForEach((ref LocalToWorld localToWorld, in BulletColliderData bulletColliderData) =>
        {
            bulletColliderRadiusArray[counter] = bulletColliderData.ColliderRadius;
            bulletColliderPositionArray[counter] = localToWorld.Position;
            counter++;
        }).WithoutBurst().Run();

        counter = 0;
        Entities.WithAll<BulletMovementData>().ForEach((Entity e) =>
        {
            bulletEntityArray[counter] = e;
            counter++;
        }).WithoutBurst().Run();

        Entities.WithAll<AsteroidMovementData>().ForEach((Entity e, int entityInQueryIndex, ref LocalToWorld localToWorld, in AsteroidMovementData asteroidMovementData) =>
        {
            for (int i = 0; i < length; i++)
            {
                float bulletColliderRadius = bulletColliderRadiusArray[i];
                float3 bulletColliderPosition = bulletColliderPositionArray[i];

                float asteroidColliderRadius = asteroidMovementData.ColliderRadius;
                float3 asteroidColliderPosition = localToWorld.Position;

                float distance = math.distance(bulletColliderPosition, asteroidColliderPosition);
                float sumRadius = bulletColliderRadius + asteroidColliderRadius;

                if (distance < sumRadius)
                {
                    asteroidEntityToBeRemoved = e;
                    MainGameManager.Instance.InstantiateAsteroidBlastPrefab(new Vector3(asteroidColliderPosition.x, asteroidColliderPosition.y, asteroidColliderPosition.z));
                    bulletEntityToBeRemoved = bulletEntityArray[i];
                    MainGameManager.Instance.AddScore(asteroidMovementData.KillScore);
                    AudioManager.Instance.PlayAudio(AudioClipTypeEnum.ASTEROID_BLAST);
                }
            }
        }).WithoutBurst().Run();

        Entities.WithAll<AsteroidMovementData>().ForEach((Entity e, int entityInQueryIndex) =>
        {
            if (e == asteroidEntityToBeRemoved)
            {
                ecb.DestroyEntity(entityInQueryIndex, e);
            }
        }).WithBurst().ScheduleParallel();

        Entities.WithAll<BulletMovementData>().ForEach((Entity e, int entityInQueryIndex) =>
        {
            if (e == bulletEntityToBeRemoved)
            {
                ecb.DestroyEntity(entityInQueryIndex, e);
            }
        }).WithBurst().ScheduleParallel();

        bulletColliderRadiusArray.Dispose();
        bulletColliderPositionArray.Dispose();
        bulletEntityArray.Dispose();
        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }
}