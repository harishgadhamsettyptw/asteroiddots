using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]

public class BulletColliderData : IComponentData
{
    public float ColliderRadius;
}