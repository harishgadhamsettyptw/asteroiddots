using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;

public class BulletSpawnSystem : SystemBase
{
    private BeginSimulationEntityCommandBufferSystem beginSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        beginSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        RequireSingletonForUpdate<GameStateData>();
    }

    protected override void OnUpdate()
    {
        GameStateData gameStateData = GetSingleton<GameStateData>();
        if (gameStateData.GameState != GameStates.InGame)
            return;

        var ecb = beginSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        bool bulletKeyPressed = false;
        float3 spaceshipPosition = float3.zero;
        quaternion spaceshipRotation = quaternion.identity;
        float3 spaceshipUp = float3.zero;
        float3 barrelPosition = float3.zero;
        PowerupTypeEnum PowerupType = PowerupTypeEnum.NONE;

        Entities.ForEach((ref PlayerInputData playerInputData, in Translation translation, in Rotation rotation, in LocalToWorld localToWorld, in PlayerPropertyData playerPropertyData) =>
        {
            if (Input.GetKeyDown(playerInputData.bulletKey))
            {
                bulletKeyPressed = true;
                spaceshipPosition = translation.Value;
                spaceshipRotation = rotation.Value;
                spaceshipUp = localToWorld.Up;
                PowerupType = playerPropertyData.PowerupType;
            }
        }).Run();

        if (bulletKeyPressed)
        {
            AudioManager.Instance.PlayAudio(AudioClipTypeEnum.PLAYER_FIRE);

            Entities.WithAll<BarrelPositionData>().ForEach((in LocalToWorld localToWorld) =>
            {
                barrelPosition = localToWorld.Position;
            }).Run();
        }

        if (bulletKeyPressed)
        {
            Entities.WithAll<ShouldSpawnTag>().ForEach((Entity e, int entityInQueryIndex, ref BulletPrefabData bulletPrefabData) =>
            {
                if (PowerupType == PowerupTypeEnum.SPLIT_BULLET)
                {
                    // Spawn bullets
                    var newEntity = ecb.Instantiate(entityInQueryIndex, bulletPrefabData.BulletPrefab);
                    ecb.AddComponent<ShouldSpawnTag>(entityInQueryIndex, newEntity);
                    ecb.SetComponent(entityInQueryIndex, newEntity, new Translation
                    {
                        Value = barrelPosition
                    });
                    ecb.SetComponent(entityInQueryIndex, newEntity, new Rotation
                    {
                        Value = math.mul(spaceshipRotation, quaternion.RotateZ(math.radians(10.1f)))
                    });

                    var newEntity1 = ecb.Instantiate(entityInQueryIndex, bulletPrefabData.BulletPrefab);
                    ecb.AddComponent<ShouldSpawnTag>(entityInQueryIndex, newEntity1);
                    ecb.SetComponent(entityInQueryIndex, newEntity1, new Translation
                    {
                        Value = barrelPosition
                    });
                    ecb.SetComponent(entityInQueryIndex, newEntity1, new Rotation
                    {
                        Value = math.mul(spaceshipRotation, quaternion.RotateZ(-math.radians(10.1f)))
                    });
                }
                else
                {
                    // Spawn bullets
                    var newEntity = ecb.Instantiate(entityInQueryIndex, bulletPrefabData.BulletPrefab);
                    ecb.AddComponent<ShouldSpawnTag>(entityInQueryIndex, newEntity);
                    ecb.SetComponent(entityInQueryIndex, newEntity, new Translation
                    {
                        Value = barrelPosition
                    });
                    ecb.SetComponent(entityInQueryIndex, newEntity, new Rotation
                    {
                        Value = spaceshipRotation
                    });
                }
            }).ScheduleParallel();
        }

        bulletKeyPressed = false;
        beginSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }
}