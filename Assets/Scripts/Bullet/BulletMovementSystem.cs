using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;

[AlwaysSynchronizeSystem]
public class BulletMovementSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float deltaTime = Time.DeltaTime;

        Entities.ForEach((ref BulletMovementData bulletMovementData,ref Translation translation, in LocalToWorld localToWorld) =>
        {
            float3 direction = localToWorld.Up * deltaTime * bulletMovementData.BulletSpeed;
            translation.Value += direction;
        }).Run();
        return default;
    }
}