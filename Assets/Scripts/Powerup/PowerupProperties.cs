using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public enum PowerupTypeEnum
{
    NONE, SHIELD, SPLIT_BULLET
}

[GenerateAuthoringComponent]
public struct PowerupProperties : IComponentData
{
    public float Timer;
    public float ColliderRadius;
    public PowerupTypeEnum PowerupType;
    public float PowerupDuration;
}