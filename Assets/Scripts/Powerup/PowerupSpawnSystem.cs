using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Random = Unity.Mathematics.Random;
using Unity.Mathematics;

[UpdateBefore(typeof(TransformSystemGroup))]
public class PowerupSpawnSystem : ComponentSystem
{
    private float deltaTime;
    private Random random;
    private float spawnDelay;
    private float timer = 0;

    protected override void OnCreate()
    {
        random = new Random(314159);
        RequireSingletonForUpdate<GameStateData>();
        RequireSingletonForUpdate<PowerupData>();
        RequireSingletonForUpdate<ScreenBoundaryData>();
    }

    protected override void OnStartRunning()
    {
        base.OnStartRunning();

        Entities.ForEach((ref PowerupData powerupData) =>
        {
            spawnDelay = powerupData.SpawnDelay;
        });
    }

    protected override void OnUpdate()
    {
        GameStateData gameStateData = GetSingleton<GameStateData>();
        if (gameStateData.GameState != GameStates.InGame)
            return;

        deltaTime = Time.DeltaTime;

        PowerupData powerupData = GetSingleton<PowerupData>();

        ScreenBoundaryData screenBoundaryData = GetSingleton<ScreenBoundaryData>();
        float minX = screenBoundaryData.MinX, minY = screenBoundaryData.MinY, maxX = screenBoundaryData.MaxX, maxY = screenBoundaryData.MaxY, minRange = screenBoundaryData.MinRange, maxRange = screenBoundaryData.MaxRange;
        float3 randomPosition = random.NextFloat3(new float3(minX, minY, 0f), new float3(maxX, maxY, 0f));
        int randomIndex = random.NextInt(100);

        timer += deltaTime;

        if (timer >= spawnDelay)
        {
            timer -= spawnDelay;
            Entity prefabToSpawn = randomIndex % 2 == 0 ? powerupData.ShieldPowerupPrefab : powerupData.BulletpowerupPrefab;
            var newEntity = PostUpdateCommands.Instantiate(prefabToSpawn);
            PostUpdateCommands.SetComponent(newEntity, new Translation
            {
                Value = randomPosition
            });
        }
    }
}