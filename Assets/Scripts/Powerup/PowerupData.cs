using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct PowerupData : IComponentData
{
    public Entity ShieldPowerupPrefab;
    public Entity BulletpowerupPrefab;
    public float SpawnDelay;
}