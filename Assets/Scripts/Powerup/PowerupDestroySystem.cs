using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class PowerupDestroySystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        RequireSingletonForUpdate<GameStateData>();
        RequireSingletonForUpdate<PlayerMovementData>();
        RequireSingletonForUpdate<PlayerPropertyData>();
    }

    protected override void OnUpdate()
    {
        GameStateData gameStateData = GetSingleton<GameStateData>();
        if (gameStateData.GameState != GameStates.InGame)
            return;

        float deltaTime = Time.DeltaTime;

        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        float playerShipColliderRadius = 0;
        float3 playerShipPosition = float3.zero;
        Entity playerEntity = default;
        Entity shieldPrefab = default;

        PlayerPropertyData playerPropertyData = GetSingleton<PlayerPropertyData>();

        Entities.ForEach((Entity e, int entityInQueryIndex, ref PlayerInputData playerInputData, ref PlayerMovementData playerMovementData, in LocalToWorld localToWorld) =>
        {
            playerShipColliderRadius = playerInputData.ColliderRadius;
            playerShipPosition = localToWorld.Position;
            playerEntity = e;
            shieldPrefab = playerMovementData.ShieldPrefab;
        }).WithoutBurst().Run();

        Entities.ForEach((Entity e, int entityInQueryIndex, ref PowerupProperties powerupProperties, in LocalToWorld localToWorld) =>
        {
            float powerupColliderRadius = powerupProperties.ColliderRadius;
            float3 powerupColliderPosition = localToWorld.Position;

            float distance = math.distance(powerupColliderPosition, playerShipPosition);
            float sumRadius = powerupColliderRadius + playerShipColliderRadius;
            
            if (distance < sumRadius)
            {
                ecb.DestroyEntity(entityInQueryIndex, e);

                if (powerupProperties.PowerupType == PowerupTypeEnum.SHIELD)
                {
                    Entity shieldEntity = default;
                    if (playerPropertyData.PowerupType == PowerupTypeEnum.SHIELD)
                        shieldEntity = playerPropertyData.ShieldEntity;
                    else
                        shieldEntity = ecb.Instantiate(entityInQueryIndex, shieldPrefab);

                    ecb.SetComponent(entityInQueryIndex, playerEntity, new PlayerPropertyData
                    {
                        PowerupType = powerupProperties.PowerupType,
                        PowerupDuration = powerupProperties.PowerupDuration,
                        ShieldEntity = shieldEntity
                    });
                }
                else if (powerupProperties.PowerupType == PowerupTypeEnum.SPLIT_BULLET)
                {
                    ecb.SetComponent<PlayerPropertyData>(entityInQueryIndex, playerEntity, new PlayerPropertyData
                    {
                        PowerupType = powerupProperties.PowerupType,
                        PowerupDuration = powerupProperties.PowerupDuration
                    });
                }
            }

            powerupProperties.Timer -= deltaTime;
            if (powerupProperties.Timer <= 0)
            {
                ecb.DestroyEntity(entityInQueryIndex, e);
            }
        }).ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }
}