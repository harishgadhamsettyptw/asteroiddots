using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Random = Unity.Mathematics.Random;

public class UFOSpawnSystem : SystemBase
{
    private float m_ElapsedTime;
    private Random random;

    protected override void OnCreate()
    {
        base.OnCreate();
        random = new Random(314159);
        RequireSingletonForUpdate<UFOPrefabData>();
        RequireSingletonForUpdate<ScreenBoundaryData>();
    }

    protected override void OnUpdate()
    {
        GameStateData gameStateData = GetSingleton<GameStateData>();
        if (gameStateData.GameState != GameStates.InGame)
            return;

        UFOPrefabData uFOPrefabData = GetSingleton<UFOPrefabData>();

        ScreenBoundaryData screenBoundaryData = GetSingleton<ScreenBoundaryData>();
        float minX = screenBoundaryData.MinX;
        float minY = screenBoundaryData.MinY;
        float maxX = screenBoundaryData.MaxX;
        float maxY = screenBoundaryData.MaxY;

        float3 randomPosition = float3.zero;
        int randomInt = random.NextInt(0, 1000);

        if (randomInt % 4 == 0)
        {
            // Left
            randomPosition = random.NextFloat3(new float3(minX, minY, 0f), new float3(minX, maxY, 0f));
        }
        else if (randomInt % 3 == 0)
        {
            // Right
            randomPosition = random.NextFloat3(new float3(maxX, minY, 0f), new float3(maxX, maxY, 0f));
        }
        else if (randomInt % 2 == 0)
        {
            // Up
            randomPosition = random.NextFloat3(new float3(minX, maxY, 0f), new float3(maxX, maxY, 0f));
        }
        else
        {
            // Down
            randomPosition = random.NextFloat3(new float3(minX, minY, 0f), new float3(maxX, minY, 0f));
        }

        m_ElapsedTime += Time.DeltaTime;
        if (m_ElapsedTime >= uFOPrefabData.SpawnDuration)
        {
            // Spawn UFO
            Entity UFOEntity = EntityManager.Instantiate(uFOPrefabData.UFOPrefab);
            EntityManager.SetComponentData(UFOEntity, new Translation
            {
                Value = randomPosition
            });

            UFOMovementData uFOMovementData = EntityManager.GetComponentData<UFOMovementData>(UFOEntity);
            float3 dir = math.normalize(float3.zero - randomPosition);
            uFOMovementData.MoveDirection = dir;
            uFOMovementData.MoveSpeed = random.NextFloat(uFOMovementData.MinSpeed, uFOMovementData.MaxSpeed);
            EntityManager.SetComponentData(UFOEntity, uFOMovementData);

            m_ElapsedTime = 0;
        }
    }
}