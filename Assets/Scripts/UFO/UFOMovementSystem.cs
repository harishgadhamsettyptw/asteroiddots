using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class UFOMovementSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        base.OnCreate();
        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        RequireSingletonForUpdate<GameStateData>();
        RequireSingletonForUpdate<PlayerMovementData>();
    }

    protected override void OnUpdate()
    {
        GameStateData gameStateData = GetSingleton<GameStateData>();
        if (gameStateData.GameState != GameStates.InGame)
            return;

        Entity uFoEntityToBeRemoved = default;
        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
        Entity playerEntity = GetSingletonEntity<PlayerMovementData>();
        Translation playerTranslation = EntityManager.GetComponentData<Translation>(playerEntity);
        float3 playerPosition = playerTranslation.Value;
        float playerColliderRadius = EntityManager.GetComponentData<PlayerInputData>(playerEntity).ColliderRadius;

        float deltaTime = Time.DeltaTime;

        Entities.ForEach((Entity e, int entityInQueryIndex, ref UFOMovementData uFOMovementData, ref Translation translation) =>
        {
            translation.Value += uFOMovementData.MoveDirection * uFOMovementData.MoveSpeed * deltaTime;

            uFOMovementData.Timer += deltaTime;

            if (uFOMovementData.Timer >= uFOMovementData.BulletSpawnInterval)
            {
                uFOMovementData.Timer = 0;
                // Spawn bullet from UFO.
                Entity bulletEntity = ecb.Instantiate(entityInQueryIndex, uFOMovementData.BulletPrefab);
                ecb.SetComponent(entityInQueryIndex, bulletEntity, new Translation
                {
                    Value = translation.Value
                });

                float3 direction = math.normalize(playerPosition - translation.Value);
                UFOBulletMovementData uFOBulletMovementData = new UFOBulletMovementData { BulletSpeed = 7, Direction = direction, ColliderRadius = 0.12f };
                ecb.SetComponent(entityInQueryIndex, bulletEntity, uFOBulletMovementData);
            }
        }).ScheduleParallel();

        bool isShieldActive = false;
        Entities.WithAll<PlayerPropertyData>().ForEach((ref PlayerPropertyData playerPropertyData) =>
        {
            if (playerPropertyData.PowerupType == PowerupTypeEnum.SHIELD)
                isShieldActive = true;
        }).Run();

        if (isShieldActive)
            return;

        Entities.ForEach((Entity e, int entityInQueryIndex, ref UFOMovementData uFOMovementData, in Translation translation) =>
        {
            float3 uFOPosition = translation.Value;
            float uFOColliderRadius = uFOMovementData.ColliderRadius;

            float distance = math.distance(playerPosition, uFOPosition);
            float sumRadius = playerColliderRadius + uFOColliderRadius;

            AudioManager.Instance.PlayContinuousAudio(AudioClipTypeEnum.UFO_MOVE);

            if (distance < sumRadius)
            {
                uFoEntityToBeRemoved = e;

                World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<RestartSystem>().ResetPlayerPosition();

                AddShield();
            }
        }).WithoutBurst().Run();

        Entities.WithAll<UFOMovementData>().ForEach((Entity e, int entityInQueryIndex) =>
        {
            if (e == uFoEntityToBeRemoved)
            {
                ecb.DestroyEntity(entityInQueryIndex, e);
            }
        }).ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }

    void AddShield()
    {
        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        Entities.ForEach((Entity e, int entityInQueryIndex, ref PlayerPropertyData playerPropertyData, in PlayerMovementData playerMovementData) =>
        {
            Entity shieldEntity = ecb.Instantiate(entityInQueryIndex, playerMovementData.ShieldPrefab);

            playerPropertyData.PowerupDuration = 3;
            playerPropertyData.PowerupType = PowerupTypeEnum.SHIELD;
            playerPropertyData.ShieldEntity = shieldEntity;

            ecb.SetComponent(entityInQueryIndex, e, playerPropertyData);
        }).ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }
}