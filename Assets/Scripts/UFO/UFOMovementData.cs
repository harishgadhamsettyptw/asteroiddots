using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct UFOMovementData : IComponentData
{
    public float MinSpeed;
    public float MaxSpeed;
    public float MoveSpeed;
    public float3 MoveDirection;

    public Entity BulletPrefab;
    public float BulletSpawnInterval;
    public float Timer;

    public float ColliderRadius;
}