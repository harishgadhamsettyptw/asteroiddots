﻿using System.Collections;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct UFOBulletMovementData : IComponentData
{
    public float BulletSpeed;
    public float3 Direction;
    public float ColliderRadius;
}