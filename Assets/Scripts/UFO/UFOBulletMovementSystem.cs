﻿using System.Collections;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class UFOBulletMovementSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        base.OnCreate();
        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;

        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        Entity bulletEntityToBeRemoved = default;
        float3 playerPosition = float3.zero;
        float playerColliderRadius = 0;
        Entity playerEntity = default;

        Entities.ForEach((ref Translation translation, in UFOBulletMovementData uFOBulletMovementData) =>
        {
            translation.Value += uFOBulletMovementData.Direction * uFOBulletMovementData.BulletSpeed * deltaTime;
        }).Run();

        bool isShieldActive = false;
        Entities.WithAll<PlayerPropertyData>().ForEach((ref PlayerPropertyData playerPropertyData) =>
        {
            if (playerPropertyData.PowerupType == PowerupTypeEnum.SHIELD)
                isShieldActive = true;
        }).Run();

        if (isShieldActive)
            return;

        Entities.ForEach((Entity e, int entityInQueryIndex, ref PlayerInputData playerInputData, ref Translation translation) =>
        {
            playerEntity = e;
            playerPosition = translation.Value;
            playerColliderRadius = playerInputData.ColliderRadius;
        }).Run();

        Entities.ForEach((Entity e, int entityInQueryIndex, ref UFOBulletMovementData uFOBulletMovementData, in Translation translation) =>
        {
            float3 bulletPosition = translation.Value;
            float bulletColliderRadius = uFOBulletMovementData.ColliderRadius;

            float distance = math.distance(playerPosition, bulletPosition);
            float sumRadius = playerColliderRadius + bulletColliderRadius;

            if (distance < sumRadius)
            {
                bulletEntityToBeRemoved = e;

                World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<RestartSystem>().ResetPlayerPosition();

                AddShield();
            }
        }).WithoutBurst().Run();

        Entities.WithAll<UFOBulletMovementData>().ForEach((Entity e, int entityInQueryIndex) =>
        {
            if (e == bulletEntityToBeRemoved)
            {
                ecb.DestroyEntity(entityInQueryIndex, e);
            }
        }).ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }

    void AddShield()
    {
        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        Entities.ForEach((Entity e, int entityInQueryIndex, ref PlayerPropertyData playerPropertyData, in PlayerMovementData playerMovementData) =>
        {
            Entity shieldEntity = ecb.Instantiate(entityInQueryIndex, playerMovementData.ShieldPrefab);

            playerPropertyData.PowerupDuration = 3;
            playerPropertyData.PowerupType = PowerupTypeEnum.SHIELD;
            playerPropertyData.ShieldEntity = shieldEntity;

            ecb.SetComponent(entityInQueryIndex, e, playerPropertyData);
        }).ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }
}