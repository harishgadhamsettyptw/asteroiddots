using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;

[AlwaysSynchronizeSystem]
public class PlayerCollisionSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        base.OnCreate();
        RequireSingletonForUpdate<GameStateData>();
        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        GameStateData gameStateData = GetSingleton<GameStateData>();
        if (gameStateData.GameState != GameStates.InGame)
            return;

        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        Entity asteroidEntityToBeRemoved = default;
        Entity playerEntity = GetSingletonEntity<PlayerInputData>();
        PlayerInputData playerInputData = GetSingleton<PlayerInputData>();

        bool isShieldActive = false;
        Entities.WithAll<PlayerPropertyData>().ForEach((ref PlayerPropertyData playerPropertyData) =>
        {
            if (playerPropertyData.PowerupType == PowerupTypeEnum.SHIELD)
                isShieldActive = true;
        }).Run();

        if (isShieldActive)
            return;

        Entities.WithAll<AsteroidMovementData>().ForEach((Entity e, int entityInQueryIndex, ref LocalToWorld localToWorld, ref Translation translation, in AsteroidMovementData asteroidMovementData) =>
        {
            float3 asteroidColliderPosition = localToWorld.Position;
            LocalToWorld playerLocalToWorld = EntityManager.GetComponentData<LocalToWorld>(playerEntity);
            float3 playerColliderPosition = playerLocalToWorld.Position;

            float playerColliderRadius = playerInputData.ColliderRadius;
            float asteroidColliderRadius = asteroidMovementData.ColliderRadius;

            float distance = math.distance(playerColliderPosition, asteroidColliderPosition);
            float sumRadius = playerColliderRadius + asteroidColliderRadius;

            if (distance < sumRadius)
            {
                asteroidEntityToBeRemoved = e;
                MainGameManager.Instance.InstantiateAsteroidBlastPrefab(new Vector3(asteroidColliderPosition.x, asteroidColliderPosition.y, asteroidColliderPosition.z));

                World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<RestartSystem>().ResetPlayerPosition();
                AddShield();
            }
        }).WithoutBurst().Run();

        Entities.WithAll<AsteroidMovementData>().ForEach((Entity e, int entityInQueryIndex) =>
        {
            if (e == asteroidEntityToBeRemoved)
            {
                ecb.DestroyEntity(entityInQueryIndex, e);
            }
        }).WithBurst().ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }

    void AddShield()
    {
        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        Entities.ForEach((Entity e, int entityInQueryIndex, ref PlayerPropertyData playerPropertyData, in PlayerMovementData playerMovementData) =>
        {
            Entity shieldEntity = ecb.Instantiate(entityInQueryIndex, playerMovementData.ShieldPrefab);

            playerPropertyData.PowerupDuration = 3;
            playerPropertyData.PowerupType = PowerupTypeEnum.SHIELD;
            playerPropertyData.ShieldEntity = shieldEntity;

            ecb.SetComponent(entityInQueryIndex, e, playerPropertyData);
        }).ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }
}