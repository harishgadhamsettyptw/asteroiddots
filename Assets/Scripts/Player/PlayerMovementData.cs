using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct PlayerMovementData : IComponentData
{
    public float3 direction;
    public float angleZ;
    public float moveSpeed;
    public float turnSpeed;

    public Entity ShieldPrefab;
}