using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct PlayerPropertyData : IComponentData
{
    public PowerupTypeEnum PowerupType;
    public float PowerupDuration;
    public Entity ShieldEntity;
}