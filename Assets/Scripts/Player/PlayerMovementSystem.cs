using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;

[AlwaysSynchronizeSystem]
public class PlayerMovementSystem : SystemBase
{
    protected override void OnCreate()
    {
        base.OnCreate();
        RequireSingletonForUpdate<PlayerMovementData>();
        RequireSingletonForUpdate<PlayerInputData>();
        RequireSingletonForUpdate<GameStateData>();
        RequireSingletonForUpdate<PlayerPropertyData>();
        RequireSingletonForUpdate<ScreenBoundaryData>();
    }

    protected override void OnUpdate()
    {
        GameStateData gameStateData = GetSingleton<GameStateData>();
        if (gameStateData.GameState != GameStates.InGame)
            return;

        bool keyPressesd = false;

        ScreenBoundaryData screenBoundaryData = GetSingleton<ScreenBoundaryData>();
        float minX = screenBoundaryData.MinX;
        float minY = screenBoundaryData.MinY;
        float maxX = screenBoundaryData.MaxX;
        float maxY = screenBoundaryData.MaxY;

        float deltaTime = Time.DeltaTime;

        var playerEntity = GetSingletonEntity<PlayerInputData>();
        PlayerInputData playerInputData = GetSingleton<PlayerInputData>();
        PlayerMovementData playerMovementData = GetSingleton<PlayerMovementData>();

        var translation = EntityManager.GetComponentData<Translation>(playerEntity);
        var rotation = EntityManager.GetComponentData<Rotation>(playerEntity);

        if (Input.GetKey(playerInputData.leftKey))
        {
            rotation.Value = math.mul(rotation.Value, quaternion.RotateZ(playerMovementData.turnSpeed * deltaTime));
            keyPressesd = true;
        }

        if (Input.GetKey(playerInputData.rightKey))
        {
            rotation.Value = math.mul(rotation.Value, quaternion.RotateZ(-playerMovementData.turnSpeed * deltaTime));
            keyPressesd = true;
        }

        float3 position = float3.zero;
        if (Input.GetKey(playerInputData.forwardKey))
        {
            position.y += playerMovementData.moveSpeed * deltaTime;
            keyPressesd = true;
        }
        else if (Input.GetKey(playerInputData.backwardKey))
        {
            position.y -= playerMovementData.moveSpeed * deltaTime;
            keyPressesd = true;
        }
        translation.Value += math.mul(rotation.Value, position);

        if (translation.Value.x <= minX)
            translation.Value.x = maxX;
        else if (translation.Value.x >= maxX)
            translation.Value.x = minX;
        else if (translation.Value.y <= minY)
            translation.Value.y = maxY;
        else if (translation.Value.y >= maxY)
            translation.Value.y = minY;

        EntityManager.SetComponentData(playerEntity, translation);
        EntityManager.SetComponentData(playerEntity, rotation);

        PlayerPropertyData playerPropertyData = GetSingleton<PlayerPropertyData>();
        if (playerPropertyData.PowerupType == PowerupTypeEnum.SHIELD)
        {
            Entity shieldEntity = playerPropertyData.ShieldEntity;
            if (shieldEntity.Index > -1)
            {
                EntityManager.SetComponentData(shieldEntity, new Translation
                {
                    Value = translation.Value
                });
                EntityManager.SetComponentData(shieldEntity, new Rotation
                {
                    Value = rotation.Value
                });
            }
        }

        if (keyPressesd)
        {
            AudioManager.Instance.PlayContinuousAudio(AudioClipTypeEnum.PLAYER_MOVE);
        }
    }
}