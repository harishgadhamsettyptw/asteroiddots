using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class PlayerPowerupSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;

        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        Entities.ForEach((Entity e, int entityInQueryIndex, ref PlayerPropertyData playerPropertyData) =>
        {
            if (playerPropertyData.PowerupType != PowerupTypeEnum.NONE)
            {
                playerPropertyData.PowerupDuration -= deltaTime;

                if (playerPropertyData.PowerupDuration <= 0)
                {
                    if (playerPropertyData.PowerupType == PowerupTypeEnum.SHIELD)
                    {
                        ecb.DestroyEntity(entityInQueryIndex, playerPropertyData.ShieldEntity);
                        playerPropertyData.ShieldEntity = Entity.Null;
                    }
                    playerPropertyData.PowerupType = PowerupTypeEnum.NONE;
                }
            }
        }).ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }
}