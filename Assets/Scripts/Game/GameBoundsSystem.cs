﻿using System.Collections;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

public class GameBoundsSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        base.OnCreate();
        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        RequireSingletonForUpdate<GameStateData>();
        RequireSingletonForUpdate<ScreenBoundaryData>();
    }

    protected override void OnUpdate()
    {
        GameStateData gameStateData = GetSingleton<GameStateData>();
        if (gameStateData.GameState != GameStates.InGame)
            return;

        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        ScreenBoundaryData screenBoundaryData = GetSingleton<ScreenBoundaryData>();
        float minX = screenBoundaryData.MinX;
        float minY = screenBoundaryData.MinY;
        float maxX = screenBoundaryData.MaxX;
        float maxY = screenBoundaryData.MaxY;
        float minRange = screenBoundaryData.MinRange;

        float deltaTime = Time.DeltaTime;

        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        Entities.WithAny<UFOMovementData>().WithAny<UFOBulletMovementData>().WithAny<AsteroidMovementData>().WithAny<BulletMovementData>().ForEach((Entity e, int entityInQueryIndex, ref Translation translation) =>
        {
            // Checking for entity position outside camera view.
            if (translation.Value.y > maxY + minRange || translation.Value.y < minY - minRange ||
                translation.Value.x > maxX + minRange || translation.Value.x < minX - minRange)
            {
                ecb.DestroyEntity(entityInQueryIndex, e);
            }
        }).ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }
}