using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct ScreenBoundaryData : IComponentData
{
    public float MinX;
    public float MinY;
    public float MaxX;
    public float MaxY;
    public float MinRange;
    public float MaxRange;

    public float AspectRatio;
    public float FOV;
}