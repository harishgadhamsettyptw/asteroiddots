using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

[UpdateAfter(typeof(SimulationSystemGroup))]
public class RestartSystem : SystemBase
{
    private EntityQuery m_AsteroidsQuery;
    private EntityQuery m_BulletQuery;
    private EntityQuery m_BulletTipQuery;

    private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;

    protected override void OnCreate()
    {
        base.OnCreate();

        RequireSingletonForUpdate<GameStateData>();
        RequireSingletonForUpdate<PlayerMovementData>();
        RequireSingletonForUpdate<PlayerPropertyData>();

        m_AsteroidsQuery = GetEntityQuery(new ComponentType[]
        {
            ComponentType.ReadOnly<AsteroidMovementData>(), ComponentType.ReadOnly<SpriteRenderer>()
        });

        m_BulletQuery = GetEntityQuery(new ComponentType[]
        {
            ComponentType.ReadOnly<BulletMovementData>(), ComponentType.ReadOnly<SpriteRenderer>()
        });

        m_BulletTipQuery = GetEntityQuery(new ComponentType[]
        {
            ComponentType.ReadOnly<BulletColliderData>()
        });

        endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnStartRunning()
    {
        UpdateGameState(GameStates.Start);
    }

    protected override void OnUpdate()
    {
        GameStateData gameStateData = GetSingleton<GameStateData>();

        if (Input.GetKeyDown(KeyCode.C))
        {
            EntityManager.DestroyEntity(m_AsteroidsQuery);
            EntityManager.DestroyEntity(m_BulletTipQuery);
            EntityManager.DestroyEntity(m_BulletQuery);
        }

        if (gameStateData.GameState == GameStates.InGame)
            return;

        bool isSpacebarPressed = Input.GetMouseButtonDown(0);

        if (isSpacebarPressed)
        {
            //EntityManager.DestroyEntity(m_AsteroidsQuery);
            //EntityManager.DestroyEntity(m_BulletTipQuery);
            //EntityManager.DestroyEntity(m_BulletQuery);

            ResetPlayerPosition(true);

            UpdateGameState(GameStates.InGame);
            AddShield();
        }
    }

    private void UpdateGameState(GameStates gameState)
    {
        GameStateData gameStateData = GetSingleton<GameStateData>();
        gameStateData.GameState = gameState;
        SetSingleton(gameStateData);
    }

    public void ResetPlayerPosition(bool resetOnGameStart = false)
    {
        Entity playerEntity = GetSingletonEntity<PlayerMovementData>();
        EntityManager.SetComponentData(playerEntity, new Translation
        {
            Value = float3.zero
        });

        EntityManager.SetComponentData(playerEntity, new Rotation
        {
            Value = quaternion.identity
        });

        MainGameManager.Instance.DecreaseLifeline();
        if (MainGameManager.Instance.GetLinelines() < 0)
        {
            MainGameManager.Instance.StartGame();
            UpdateGameState(GameStates.Start);
        }

        if (!resetOnGameStart)
        {
            AudioManager.Instance.PlayAudio(AudioClipTypeEnum.PLAYER_DEAD);
        }
    }

    void AddShield()
    {
        var ecb = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        Entities.ForEach((Entity e, int entityInQueryIndex, ref PlayerPropertyData playerPropertyData, in PlayerMovementData playerMovementData) =>
        {
            Entity shieldEntity = ecb.Instantiate(entityInQueryIndex, playerMovementData.ShieldPrefab);

            playerPropertyData.PowerupDuration = 3;
            playerPropertyData.PowerupType = PowerupTypeEnum.SHIELD;
            playerPropertyData.ShieldEntity = shieldEntity;

            ecb.SetComponent(entityInQueryIndex, e, playerPropertyData);
        }).ScheduleParallel();

        endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
    }
}