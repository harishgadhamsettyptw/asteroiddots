using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class ScreenBoundarySystem : ComponentSystem
{
    protected override void OnStartRunning()
    {
        Entities.ForEach((ref ScreenBoundaryData screenBoundaryData) =>
        {
            screenBoundaryData.MinX = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, -Camera.main.transform.position.z)).x;
            screenBoundaryData.MinY = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, -Camera.main.transform.position.z)).y;

            screenBoundaryData.MaxX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, -Camera.main.transform.position.z)).x;
            screenBoundaryData.MaxY = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, -Camera.main.transform.position.z)).y;

            screenBoundaryData.AspectRatio = Camera.main.aspect;
            screenBoundaryData.FOV = Camera.main.fieldOfView;
        });
    }

    protected override void OnUpdate()
    {

    }
}