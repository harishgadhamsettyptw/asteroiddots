using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public enum GameStates
{
    None,
    Start,
    InGame
}

[GenerateAuthoringComponent]
public struct GameStateData : IComponentData
{
    public GameStates GameState;
}