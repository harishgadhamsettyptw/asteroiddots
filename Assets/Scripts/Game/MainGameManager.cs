using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainGameManager : Singleton<MainGameManager>
{
    [SerializeField] protected TextMeshProUGUI m_LivesText;

    [SerializeField] protected TextMeshProUGUI m_ScoreText;

    [SerializeField] protected int m_TotalLifelines;

    [SerializeField] protected GameObject m_BlastPrefab;

    private int currentScore = 0;

    private int currentLifelines;

    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DecreaseLifeline()
    {
        --currentLifelines;
        UpdateLivesUI(currentLifelines + 1);
    }

    public void StartGame()
    {
        currentLifelines = m_TotalLifelines;
        currentScore = 0;
        UpdateLivesUI(m_TotalLifelines);
        UpdateScoreUI(currentScore);
    }

    public int GetLinelines()
    {
        return currentLifelines;
    }

    public void AddScore(int score)
    {
        currentScore += score;
        UpdateScoreUI(currentScore);
    }

    void UpdateLivesUI(int noOfLives)
    {
        m_LivesText.text = string.Format("Lives left : {0}", noOfLives);
    }

    void UpdateScoreUI(int score)
    {
        m_ScoreText.text = string.Format("Score : {0}", score);
    }

    public void InstantiateAsteroidBlastPrefab(Vector3 position)
    {
        GameObject instObj = Instantiate(m_BlastPrefab, position, Quaternion.identity);
        Destroy(instObj, 2.0f);
    }
}