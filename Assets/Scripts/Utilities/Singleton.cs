using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : Object
{
    protected static T m_Instance;

    public static T Instance
    {
        get
        {
            if (m_Instance == null)
                m_Instance = FindObjectOfType(typeof(T)) as T;

            if (m_Instance == null)
                Debug.LogWarning(string.Format("No Singleton of type {0} found in scene", typeof(T).Name));

            return m_Instance;
        }
    }
}